#!/usr/bin/perl

use warnings;
use strict;
use Cwd;
use Cwd 'abs_path';

# This is a perl script to determine the haplotype from
# a sorted SAM file of aligned reads from an RNA-seq
# experiments.
# Author: Sean D. Gallaher
# Date: 15-SEP-2015
# Filename: sam2haplotype
# version: v1.2



#################
# This script requires two .tsv files 
# that should be in a folder called data
# that should be with the script. This 
# determines the path to them.

my $script = $0;
my $scriptAbsPath = abs_path ($script) ;
$scriptAbsPath =~ m/(.+)\/[^\/]+$/;
my $pathToScript = $1;


my $tableName = $pathToScript .  "/data/hap2_SNV_table.tsv";
my $rangesName = $pathToScript .  "/data/hap_block_key.tsv";

####################
# Check for a user supplied .SAM file name,
# or, if -h|--help is specified, print the help dialog.

my $sam = shift @ARGV;
my $help = "\nThis is a script that takes a sorted .SAM \nfile of aligned RNA-seq reads and uses it \nto determine the haplotype of the strain \nthat was used. The output consists of a summary \nwhich calls hap1, hap2, or ambiguous for\neach block, and a report \nin .tsv format that includes the number of \nhap1 and hap2 base calls in 100 variant windows.  \nThis can be viewed in MS Excel or equivalent. \nUse the data to make a 100% stacked column chart.\n\n";


if (!defined $sam) {
	die "\nPlease specify the file name of a sorted \n.SAM file of aligned RNA-seq reads\n\n";
}
elsif ($sam eq "-h") {
	die "$help";
}
elsif ($sam eq "--help") { 
	die "$help";
}

if ($sam !~ m/(.+)\.sam$/) {
	die "\nThe file must be a sorted .SAM format file,\nand the file name should end with \".sam\"\nSearch the internet for samtools if you need help\nwith this.\n\n"; 
}
my $root = $1;

####################
# The hap1 and hap2 SNVs are all in a .tsv file.
# This reads the file and puts the data
# into a hash called %table.

open (TABLE , "<", $tableName) 
	or die "Cannot open $tableName: $!\n";

my %table ;


while (my $snv = <TABLE>) {
	chomp $snv;
	if ($snv !~ m/^#/) {
		my @snvInfo = split (/\t/, $snv);
		my $block = $snvInfo[0];
		my $chrom = $snvInfo[1];
		my $pos =  $snvInfo[2];
		my $hap1 = $snvInfo[3];
		my $hap2 = $snvInfo[4];
		$table{$chrom}{$block}{$pos}{'hap1'} = $hap1;
		$table{$chrom}{$block}{$pos}{'hap2'} = $hap2;
	}
}

close (TABLE);
###################
# Another file has the chromosome positions
# of all of the blocks. This will be read
# and put into a hash called %ranges.
# @blockOrder is an array to make sure that
# the final report has the blocks in the correct
# order. 
# %blockChrom is a matrix to specify the 
# correct chromosome for each block.



open (RANGES , "<" , $rangesName)
	or die "Cannot open $rangesName: $!\n";

my %ranges;
my @blockOrder;
my %blockChrom;

while (my $block = <RANGES>) {
	chomp $block;
	if ($block !~ m/^#/) {
		my @blockInfo = split (/\t/, $block);
		my $name = $blockInfo[0];
		my $chrom = $blockInfo[1];
		my $start = $blockInfo[2] ;
		my $end = $blockInfo[3] ;
		$ranges{$chrom}{$name}{'start'} = $start;
		$ranges{$chrom}{$name}{'end'} = $end;
		push @blockOrder , $name;
		my @blockHits;
		my $blockHitsRef = \@blockHits;
		$table{$chrom}{$name}{'hitsArray'} = $blockHitsRef;
		$blockChrom{$name} = $chrom;
	}
}

close (RANGES);

##################
# Next, the program will open the .SAM file and
# start going through it line by line.
# See if the read falls within the boundaries
# of a haplotype block. If yes, go through the
# non-soft-clipped base calls and see if
# they include know SNVs. Determine if the base
# call is hap1, hap2, N, or other, and 
# add that to the hash %results.

my %results;

open (SAM , "<", $sam) 
	or die "Cannot open $sam: $!\n";

while (my $line = <SAM>) {
	chomp $line;
	if ($line !~ m/^@/) {
		my @read = split (/\t/, $line);
		my $chrom = $read[2];
		my $pos = $read[3];
		my $cigar = $read[5];
		my $read = $read[9];
		my $readLength = length $read;
		if (exists $ranges{$chrom}) {
			foreach my $block (keys %{$ranges{$chrom}}) {
				my $blockStart = $ranges{$chrom}{$block}{'start'};
				my $blockEnd = $ranges{$chrom}{$block}{'end'};
				my $adjBlockStart = $blockStart - $readLength;
				if ($pos >= $adjBlockStart ) {
					if ($pos <= $blockEnd) {
						my @cigarChars = split (//,$cigar);						
						my @cigarNums;
						my @cigarTypes;
						my $lastNum = 0;
						foreach my $char (@cigarChars) {
							if ($char =~ m/\d/) {
								if ($lastNum == 0) {
									$lastNum = $char;
								}
								elsif ($lastNum > 0) {
									$lastNum = $lastNum * 10;
									$lastNum += $char;
								}
							}
							elsif ($char =~ m/[A-Z]/) {
								push @cigarTypes , $char;
								push @cigarNums , $lastNum;
								$lastNum = 0;
							}
						}
						my @readSeq = split (//, $read);
						my $cigarTypesLength = scalar @cigarTypes;
						for (my $i = 0 ; $i < $cigarTypesLength; $i++) {
							my $count = shift @cigarNums;
							my $type = shift @cigarTypes;
							# S = soft clipped => remove
							if ($type eq "S") {
								$pos += $count;
								for (my $j = 0 ; $j < $count; $j++) {
									my $null = shift @readSeq;
								}
							}
							# M = match => check against %table
							elsif ($type eq "M") {
								for (my $m =0 ; $m < $count; $m++) {
									my $nt = shift @readSeq;
									if (defined $table{$chrom}{$block}{$pos}) {
										my $paddedPos = sprintf ("%09d", $pos);
										$results{$block}{'padded'}{$paddedPos} = "hit";
										if ($nt eq $table{$chrom}{$block}{$pos}{'hap1'}) {
											$results{$block}{'raw'}{$pos}{'hap1'}++;
										}
										elsif ($nt eq $table{$chrom}{$block}{$pos}{'hap2'}) {
											$results{$block}{'raw'}{$pos}{'hap2'}++;
										}
										elsif ($nt eq "N") {
											$results{$block}{'raw'}{$pos}{'N'}++;
										}
										else {
											$results{$block}{'raw'}{$pos}{'other'}++;
										}
									}
									$pos++;
								}
							}
							# N = intron => move $pos up 
							elsif ($type eq "N") {
								$pos += $count;
							}
							# I = insertion => move @readSeq up
							elsif ($type eq "I") {
								for (my $l = 0 ; $l < $count; $l++) {
									my $null = shift @readSeq;
								}
							}
							# D = deletion => mov $pos up
							elsif ($type eq "D") {
								$pos += $count;
							}
							

						}	
					}
				}
			}
		}
	}
}

close (SAM);


########################
# Average the positons in pools of 100 each. 
# Print the results out to a report that can be used to make a chart in Excel.
# Also, take the average for each block. If it is > 80% hap1 or hap2,
# report that to the summary file.


my $reportName = $root . ".report.tsv";
my $summaryName = $root . ".summary.tsv";

open (REPORT , ">" , $reportName)
	or die "Cannot open $reportName: $!\n";

print REPORT "#block\thap1\thap2\tN\tother\n";

open (SUMMARY , ">" , $summaryName)
	or die "Cannot open $summaryName: $!\n";

foreach my $block (@blockOrder) {
	my $totHap1calls = 0;
	my $totHap2calls = 0;
	my $posNum = 0 ;
	my $hap1 = 0;
	my $hap2 = 0;
	my $noCall = 0;
	my $other = 0;
	my $chrom = $blockChrom{$block};
	my $firstLine = 0;
	if (defined $results{$block}) {
		foreach my $pos (sort keys %{$results{$block}{'padded'}}) {
			$pos =~ s/^0+//g;
			my $anyHits = 0;
			if (defined $results{$block}{'raw'}{$pos}{'hap1'}) { 
				$hap1 += $results{$block}{'raw'}{$pos}{'hap1'} ;
				$totHap1calls += $results{$block}{'raw'}{$pos}{'hap1'} ;
				$anyHits++;
			}	
			if (defined $results{$block}{'raw'}{$pos}{'hap2'}) { 
				$hap2 += $results{$block}{'raw'}{$pos}{'hap2'} ;
				$totHap2calls += $results{$block}{'raw'}{$pos}{'hap2'} ;
				$anyHits++;
			}
			if (defined $results{$block}{'raw'}{$pos}{'N'}) { 
				$noCall += $results{$block}{'raw'}{$pos}{'N'} ;
				$anyHits++;
			}
			if (defined $results{$block}{'raw'}{$pos}{'other'}) { 
				$other += $results{$block}{'raw'}{$pos}{'other'} ;
				$anyHits++;
			}
			if ($anyHits > 0) {
				$posNum++;
			}
			my $mod = $posNum % 100;
			if ($mod == 0) {		
				if ($firstLine == 0) {
					print REPORT "$block\t$hap1\t$hap2\t$noCall\t$other\n";
					$firstLine++;
				}
				else {
					print REPORT "\t$hap1\t$hap2\t$noCall\t$other\n";
				}
				$hap1 = 0;
				$hap2 = 0;
				$noCall = 0;
				$other = 0;

			}
		}
		print REPORT "\t$hap1\t$hap2\t$noCall\t$other\n\n\n\n\n";
	}
	my $altBlockName;
	if ($block =~ m/^\d-/) {
		$altBlockName = "chr0" . "$block";
	}
	elsif ($block =~ m/^\d\d-/) { 
		$altBlockName = "chr" . "$block";
	}
	my $hapCall = 1;
	my $totCalls = $totHap1calls + $totHap2calls;
	if ($totCalls > 0) { 
		my $percentHap2 = $totHap2calls / $totCalls ; 
		if ($percentHap2 >= 0.8) {
			$hapCall = 2;
		}
		elsif ($percentHap2 >= 0.2) {
			$hapCall = 'inconclusive';
		}
	}
	print SUMMARY "$hapCall\t$altBlockName\t\# $chrom: $ranges{$chrom}{$block}{'start'}-$ranges{$chrom}{$block}{'end'}\n";
}

close (REPORT);









