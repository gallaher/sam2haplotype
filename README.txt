sam2haplotype README.txt

We have observed that many RNA-seq studies conducted in 
the algal speicies Chlamydomonas reinhardtii
were performed in strains other than what was reported
in the liturature. 
Through genomic resequencing of many commonly 
used laboratory strains, we have determined that 
the genomes of the standard lab strains consist  
different distributions of two highly divergent
(~2%) haplotypes in a mosaic pattern.  
The upshot of this is that it 
is easy to positively identify any of the standard
laboratory strains by its distribution of these
two haplotypes. 

For more details, see:

    Gallaher et al. (2015). Plant Cell 10.1105/tpc.15.00508

Fortunately for invetigators that wish to confirm 
(or find out) which strain they are using, the 
haplotype data can be used to positively 
determine this. sam2haplotype is a software tool 
designed to make it dead easy to do this. 
It takes a .SAM file of aligned RNA-seq reads 
as its only input and scans it to determine 
the haplotype of the originating 
strain. Note, in this version, sam2haplotype expects
a .SAM file of reads aligned to v5 of the 
C.reinhardtii genome.

The program outputs two files:
1) A summary file.
   This file indicates whether the strain has
   haplotype 1 or haplotype 2 for each of the 
   known blocks. If it is uncertain, it will
   report the block is inconclusive.

2) A report file that indicates the number of
   haplotype 1 and haplotype 2 calls in
   windows of 100 variants. This facilitates
   troubleshooting if necessary. Also, if the 
   strain being examined has had a recombination
   event between the two haplotypes in a single
   block, it should be obvious from this file.
   Open the file in MS Excel or equivalent 
   and plot the hap1 and hap2 calls as a
   100% stacked column chart. 

Note that the summary file is formatted to 
use as the input for the Custom Chlamy Generator
available at: 
  https://bitbucket.org/gallaher/custom-chlamy-generator
This will generate a strain-specific reference
genome that can be used to more accurately 
re-map the RNA-seq reads.

Email the author, Sean Gallaher, with questions or
comments at his last name at chem.ucla.edu


